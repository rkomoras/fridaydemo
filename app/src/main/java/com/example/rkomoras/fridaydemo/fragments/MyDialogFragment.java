package com.example.rkomoras.fridaydemo.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rkomoras.fridaydemo.R;

public class MyDialogFragment extends DialogFragment {

	public static String TAG = "MY_DIALOG_FRAGMENT";

	public MyDialogFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_dialog, container);
		return view;
	}

}
