package com.example.rkomoras.fridaydemo;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import timber.log.Timber;

public class MainActivity extends LoggedActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void openFragment(View view) {
		Intent intent = new Intent(this, FragmentListActivity.class);
		startActivity(intent);
	}

	public void openDialogFragment(View view) {
		Intent intent = new Intent(this, DialogFragmentActivity.class);
		startActivity(intent);
	}
}
