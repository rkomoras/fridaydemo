package com.example.rkomoras.fridaydemo;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;

import com.example.rkomoras.fridaydemo.fragments.MyDialogFragment;

public class DialogFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new LinearLayoutCompat(this));
        showDialogFragment();
    }

    private void showDialogFragment() {
        FragmentManager manager = getSupportFragmentManager();
        MyDialogFragment myDialogFragment = new MyDialogFragment();
        myDialogFragment.show(manager, MyDialogFragment.TAG);
    }
}
