package com.example.rkomoras.fridaydemo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import timber.log.Timber;

public abstract class LoggedFragment extends Fragment {
	@Override
	public void onAttach(Context context) {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onAttach");
		super.onAttach(context);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onStart");
		super.onStart();
	}

	@Override
	public void onResume() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onResume");
		super.onResume();
	}

	@Override
	public void onPause() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onStop");
		super.onStop();
	}

	@Override
	public void onDestroy() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onDestroy");
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onDestroyView");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onDetach");
		super.onDetach();
	}
}
