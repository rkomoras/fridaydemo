package com.example.rkomoras.fridaydemo;

import android.app.Application;
import android.content.res.Configuration;

import timber.log.Timber;

import static timber.log.Timber.DebugTree;

public class FridayDemoApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();

		if (BuildConfig.DEBUG) {
			Timber.plant(new DebugTree());
		} else {
			// TODO
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}
}
