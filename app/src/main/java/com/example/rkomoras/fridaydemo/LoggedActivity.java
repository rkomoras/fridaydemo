package com.example.rkomoras.fridaydemo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import timber.log.Timber;

public class LoggedActivity extends AppCompatActivity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onStart");
		super.onStart();
	}

	@Override
	protected void onPause() {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onPause");
		super.onPause();
	}

	@Override
	protected void onResume() {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onResume");
		super.onResume();
	}

	@Override
	protected void onStop() {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onStop");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Timber.tag(this.getClass().getSimpleName()).i("==A== onDestroy");
		super.onDestroy();
	}
}
