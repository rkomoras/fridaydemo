package com.example.rkomoras.fridaydemo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rkomoras.fridaydemo.R;

import timber.log.Timber;


public class FirstFragment extends LoggedFragment {

	public FirstFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		Timber.tag(this.getClass().getSimpleName()).i("==F== onCreateView");
		return inflater.inflate(R.layout.fragment_first, container, false);
	}
}
