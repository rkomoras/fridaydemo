package com.example.rkomoras.fridaydemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.example.rkomoras.fridaydemo.fragments.FirstFragment;
import com.example.rkomoras.fridaydemo.fragments.LoggedFragment;
import com.example.rkomoras.fridaydemo.fragments.SecondFragment;
import com.example.rkomoras.fridaydemo.fragments.ThirdFragment;

import timber.log.Timber;

public class FragmentListActivity extends LoggedActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_list);

		if (findViewById(R.id.fragmentFrameLayout) != null) {
			if (savedInstanceState != null) {
				return;
			}

			FirstFragment firstFragment = new FirstFragment();
			getSupportFragmentManager()
					.beginTransaction()
					.add(R.id.fragmentFrameLayout, firstFragment, "1")
					.commit();
		}
	}

	public void switchToNextFragment(View view) {
		switchFragment(view, true);
	}

	public void switchToPreviousFragment(View view) {
		switchFragment(view, false);
	}

	private void switchFragment(View view, boolean next) {
		Timber.tag(getClass().getSimpleName()).i("FRAGMENT SWITCH " + (next ? "NEXT" : "PREVIOUS"));
		int fragmentNumber = getFragmentNumberFromTag(view) + (next ? 1 : -1);
		String fragmentTag = Integer.toString(fragmentNumber);
		FragmentManager manager = getSupportFragmentManager();
		Fragment fragment = manager.findFragmentByTag(fragmentTag);
		if (fragment == null) {
			Timber.tag(getClass().getSimpleName()).i("FRAGMENT NOT FOUND");
			LoggedFragment nFragment = getFragmentByNumber(fragmentNumber);
			manager.beginTransaction()
					.replace(R.id.fragmentFrameLayout, nFragment, fragmentTag)
					.addToBackStack(Integer.toString(fragmentNumber))
					.commit();
		} else {
			Timber.tag(getClass().getSimpleName()).i("FRAGMENT FOUND");
			manager.popBackStack();
			manager.beginTransaction()
					.replace(R.id.fragmentFrameLayout, fragment, fragmentTag)
					.commit();
		}
	}

	public static int getFragmentNumberFromTag(View view) {
		return Integer.parseInt((String) view.getTag());
	}

	public static LoggedFragment getFragmentByNumber(int number) {
		LoggedFragment newFragment = null;
		switch (number) {
			case 1:
				newFragment = new FirstFragment();
				break;
			case 2:
				newFragment = new SecondFragment();
				break;
			case 3:
				newFragment = new ThirdFragment();
				break;
		}
		return newFragment;
	}
}
